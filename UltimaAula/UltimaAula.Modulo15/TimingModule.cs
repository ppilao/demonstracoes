﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace UltimaAula.Modulo15
{
    public class TimingModule : IHttpModule
    {
        public void Dispose()
        {
            HttpContext.Current.Items["sw"] = null;
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;
            context.EndRequest += context_EndRequest;
        }
        void context_BeginRequest(object sender, EventArgs e)
        {
            var sw = new Stopwatch();
            HttpContext.Current.Items["sw"] = sw;
            sw.Start();
        }
        void context_EndRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.Response.ContentType == "text/html")
            {
                var sw = (Stopwatch)HttpContext.Current.Items["sw"];
                sw.Stop();
                var mensagem = string.Format("A requisição demorou {0} milisegundos",
                    sw.Elapsed.TotalMilliseconds);

                HttpContext.Current.Response.Write(string.Format("<h1 style=color:red>{0}</h1>", mensagem));
            }
        }
    }
}