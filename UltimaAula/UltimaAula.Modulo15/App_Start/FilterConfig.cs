﻿using System.Web;
using System.Web.Mvc;

namespace UltimaAula.Modulo15
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}