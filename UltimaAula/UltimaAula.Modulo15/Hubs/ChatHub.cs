﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimaAula.Modulo15.Hubs
{
    public class ChatHub:Hub
    {
        public void Enviar(string nome, string mensagem) {
            Clients.All.enviarMensagem(nome, mensagem);
        }
    }
}