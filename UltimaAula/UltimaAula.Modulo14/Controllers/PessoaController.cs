﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UltimaAula.Modulo14.Models;

namespace UltimaAula.Modulo14.Controllers
{
    public class PessoaController : ApiController
    {
        // GET api/pessoa
        public IEnumerable<Pessoa> Get()
        {
            return new Pessoa().Pessoas();
        }

        // GET api/pessoa/5
        public HttpResponseMessage Get(int id)
        {
            var pessoa = new Pessoa().Pessoas().FirstOrDefault(x => x.Id == id);
            if (pessoa != null)
                return Request.CreateResponse(HttpStatusCode.OK, pessoa);
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        // POST api/pessoa
        public void Post([FromBody]Pessoa pessoa)
        {

        }

        // PUT api/pessoa/5
        public void Put(int id, [FromBody]Pessoa pessoa)
        {
        }

        // DELETE api/pessoa/5
        public void Delete(int id)
        {
        }
    }
}
