﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UltimaAula.Modulo14.Models
{
    public class Pessoa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }

        public IEnumerable<Pessoa> Pessoas() {
            yield return new Pessoa { Id = 1, Nome = "Paulo", Email = "paulo@hotmail.com" };
            yield return new Pessoa { Id = 2, Nome = "Agnes", Email = "agnes@hotmail.com" };
        }
    }
}