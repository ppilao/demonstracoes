﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace UltimaAula.Modulo13WCF
{
    public class MensagemService : IMensagemService
    {
        public string Mensagem()
        {
            return "Mensagem enviada do WCF!!!";
        }
    }
}
