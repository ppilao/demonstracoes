﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace CadastroDeBandas.ActionFilters
{
    public class LoggerFilter: ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Debug.WriteLine("Ação terminou às " + DateTime.Now.ToLongTimeString());

            base.OnActionExecuted(filterContext);
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Debug.WriteLine("Ação começou às " + DateTime.Now.ToLongTimeString());

            base.OnActionExecuting(filterContext);
        }
    }
}