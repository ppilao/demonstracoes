﻿using CadastroDeBandas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CadastroDeBandas.Controllers
{
    public class MusicaController : Controller
    {
        //
        // GET: /Musica/

        public ActionResult Index(int id)
        {
            var repositorio = new Repositorio();
            var musicas = repositorio.ListarMusicas(id);
            return View(musicas);
        }

        //
        // GET: /Musica/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Musica/Create

        public ActionResult Create(int id)
        {
            var musica = new Musica { BandaId = id };
            return View(musica);
        }

        //
        // POST: /Musica/Create

        [HttpPost]
        public ActionResult Create(Musica musica)
        {
            try
            {
                new Repositorio().IncluirMusica(musica);

                return RedirectToAction("Index","Banda");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Musica/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Musica/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Musica/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Musica/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
