﻿using CadastroDeBandas.ActionFilters;
using CadastroDeBandas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CadastroDeBandas.Controllers
{
    public class BandaController : Controller
    {
        public PartialViewResult PartialMusicas(int id) {
            var musicas = new Repositorio().ListarMusicas(id);
            return PartialView( musicas);
        }

        public ActionResult ExemploHelpers() {
            return View(new Banda { Pais="Brasil"});
        }

        public string ActionTeste() {
            return "Esta é uma mensagem bla bla bla bla bla";
        }

        public ActionResult Index()
        {
            var bandas = new Repositorio().ListarBandas();
            return View(bandas);
        }


        [LoggerFilter]
        public ActionResult Details(int id)
        {
            var banda = new Repositorio().BuscarBanda(id);
            return View(banda);
        }
        //[Authorize]
        //[OutputCache(Duration=60)]
        public ActionResult Details2(string pais,string cor)
        {
            System.Threading.Thread.Sleep(10000);

            var banda = new Repositorio().BuscarBandaPorPais(pais).FirstOrDefault();

            ViewBag.texto = "Você escolheu a cor " + cor + " para a banda " + banda.Nome;
            ViewBag.color = cor;
            return View("Details",banda);
        }
        //
        // GET: /Banda/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Banda/Create

        [HttpPost]
        public ActionResult Create(Banda banda)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    new Repositorio().IncluirBanda(banda);
                    return RedirectToAction("Index");
                }

                return View("ExemploHelpers");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Banda/Edit/5

        public ActionResult Edit(int id)
        {
            var banda = new Repositorio().BuscarBanda(id);
            return View(banda);
        }

        //
        // POST: /Banda/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, Banda banda)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    new Repositorio().AlterarBanda(banda);
                    return RedirectToAction("Index");
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Banda/Delete/5

        public ActionResult Delete(int id)
        {
            var banda = new Repositorio().BuscarBanda(id);
            return View(banda);
        }

        //
        // POST: /Banda/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                new Repositorio().ExcluirBanda(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
