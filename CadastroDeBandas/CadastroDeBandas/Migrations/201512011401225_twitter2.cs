namespace CadastroDeBandas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class twitter2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Banda", "Twitter", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Banda", "Twitter", c => c.String());
        }
    }
}
