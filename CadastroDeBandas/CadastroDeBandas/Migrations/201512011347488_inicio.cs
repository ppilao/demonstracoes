namespace CadastroDeBandas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Banda",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        AnoFormacao = c.Int(nullable: false),
                        Estilo = c.String(),
                        Pais = c.String(),
                        Classificacao = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Musicas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Album = c.String(),
                        BandaId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Banda", t => t.BandaId)
                .Index(t => t.BandaId);
            
            CreateTable(
                "dbo.Musicos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Instrumento = c.String(),
                        BandaId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Banda", t => t.BandaId)
                .Index(t => t.BandaId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Musicos", new[] { "BandaId" });
            DropIndex("dbo.Musicas", new[] { "BandaId" });
            DropForeignKey("dbo.Musicos", "BandaId", "dbo.Banda");
            DropForeignKey("dbo.Musicas", "BandaId", "dbo.Banda");
            DropTable("dbo.Musicos");
            DropTable("dbo.Musicas");
            DropTable("dbo.Banda");
        }
    }
}
