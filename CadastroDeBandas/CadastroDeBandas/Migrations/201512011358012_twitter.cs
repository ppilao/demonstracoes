namespace CadastroDeBandas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class twitter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Banda", "Twitter", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Banda", "Twitter");
        }
    }
}
