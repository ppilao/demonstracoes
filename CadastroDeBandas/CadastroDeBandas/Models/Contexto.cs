﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CadastroDeBandas.Models
{
    public class Contexto:DbContext
    {
        public Contexto() : base("conexao") { }
        public Contexto(string conexao) : base(conexao) { }
        public DbSet<Banda> Bandas { get; set; }
        public DbSet<Musica> Musicas { get; set; }
        public DbSet<Musico> Musicos { get; set; }
    }
}