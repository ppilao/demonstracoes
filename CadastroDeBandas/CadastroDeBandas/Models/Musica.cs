﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CadastroDeBandas.Models
{
    [Table("Musicas")]
    public class Musica
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Album { get; set; }

        [ForeignKey("Banda")]
        public int? BandaId { get; set; }
        public Banda Banda { get; set; }
    }
}