﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CadastroDeBandas.Models
{
    [Table("Banda")]
    public class Banda
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Campo nome é obrigatório")]
        public string Nome { get; set; }
        [DisplayName("Ano de formação")]
        [Required(ErrorMessage = "Campo Ano de formação é obrigatório")]
        public int AnoFormacao { get; set; }
        //[DataType(DataType.MultilineText)]
        public string Estilo { get; set; }
        [DisplayName("País")]
        public string Pais { get; set; }
        [DisplayName("Classificação")]
        [Required(ErrorMessage = "Campo classificação é obrigatório")]
        [Range(0,5,ErrorMessage="A classificação deve ser de 0 a 5")]
        [ValidaPar(ErrorMessage="A classificação deve ser par")]
        public int Classificacao { get; set; }

        [StringLength(100)]
        public string  Twitter { get; set; }

        public virtual ICollection<Musica> Musicas { get; set; }
        public virtual ICollection<Musico> Integrantes { get; set; }

        public IEnumerable<Banda> ListarBandas()
        {
            yield return new Banda { Id = 1, Nome = "Iron Maiden", AnoFormacao = 1980, Classificacao = 5, Estilo = "Rock", Pais = "Inglaterra" };
            yield return new Banda { Id = 2, Nome = "Dream Theater", AnoFormacao = 1988, Classificacao = 5, Estilo = "Rock", Pais = "EUA" };
        }
    }
}