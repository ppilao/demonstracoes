﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CadastroDeBandas.Models
{
    public class Repositorio
    {
        private Contexto ctx = new Contexto("conexao2");
        public Banda BuscarBanda(int id)
        {
            return ctx.Bandas.Find(id);
        }
        public void IncluirBanda(Banda banda)
        {
            ctx.Bandas.Add(banda);
            ctx.SaveChanges();
        }
        public void ExcluirBanda(int id)
        {
            ctx.Bandas.Remove(BuscarBanda(id));
            ctx.SaveChanges();
        }
        public IEnumerable<Banda> ListarBandas()
        {
            return ctx.Bandas;
        }
        public IEnumerable<Musica> ListarMusicas(int bandaId)
        {
            return ctx.Musicas.Where(x => x.BandaId == bandaId);
        }
        public void IncluirMusica(Musica musica) {
            ctx.Musicas.Add(musica);
            ctx.SaveChanges();
        }
        public void AlterarBanda(Banda banda)
        {
            ctx.Entry(banda).State = System.Data.EntityState.Modified;
            ctx.SaveChanges();
        }

        public IEnumerable<Banda> BuscarBandaPorPais(string pais)
        {
            //linq
            var query = from b in ctx.Bandas
                        where b.Pais == pais
                        select b;
            return query;

            //lambda
            return ctx.Bandas.Where(x => x.Pais == pais);
        }
        public IEnumerable<Musica> BuscarMusicaPorBandaId(int id)
        {
            return ctx.Musicas.Where(x => x.BandaId == id);
        }
        public Musico BuscarMusico(int id)
        {
            return ctx.Musicos.Find(id);
        }
        public Musica BuscarMusica(int id)
        {
            return ctx.Musicas.Find(id);
        }



    }
}