﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using System.Collections.Generic;

namespace Aula3.Tests
{
    public class NumeroRomano
    {
        private string _algarismoRomano;
        private Dictionary<string, int> _mapa = 
            new Dictionary<string, int> 
        { 
            {"I",1},
            {"X",10},
            {"V",5},
            {"L",50},
            {"C",100},
        };
        public NumeroRomano(string algarismoRomano)
        {
            _algarismoRomano = algarismoRomano;
        }
        public int Conversor()
        {
            return _mapa[_algarismoRomano];
        }
    }
    [TestClass]
    public class Test1
    {
        [TestMethod]
        public void DadoQueOAlgarismoEh_I_ORetornoDeveSer_1()
        {
            var nr = new NumeroRomano("I");
            var valorEsperado = 1;
            Assert.AreEqual(valorEsperado, nr.Conversor());
        }
        [TestMethod]
        public void DadoQueOAlgarismoEh_V_ORetornoDeveSer_5()
        {
            var nr = new NumeroRomano("V");
            var valorEsperado = 5;
            Assert.AreEqual(valorEsperado, nr.Conversor());
        }
        [TestMethod]
        public void DadoQueOAlgarismoEh_X_ORetornoDeveSer_10()
        {
            var nr = new NumeroRomano("X");
            var valorEsperado = 10;
            Assert.AreEqual(valorEsperado, nr.Conversor());
        }
        [TestMethod]
        public void DadoQueOAlgarismoEh_L_ORetornoDeveSer_50()
        {
            var nr = new NumeroRomano("L");
            var valorEsperado = 50;
            Assert.AreEqual(valorEsperado, nr.Conversor());
        }
    }
}
