﻿using Aula3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Aula3.Controllers
{
    public class ProdutoController : Controller
    {
        //
        // GET: /Produto/

        public ActionResult Detalhe(int id)
        {
            var produto = new Produto
            {
                Id = id,
                Nome = "Produto " + id
            };
            return View(produto);
        }

        public ActionResult Detalhe2(int id,string categoria)
        {
            var produto = new Produto
            {
                Id = id,
                Nome = "Produto " + id,
                Categoria = categoria
            };
            return View("Detalhe",produto);
        }

    }
}
