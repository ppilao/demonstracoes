﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Aula3
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(name: "Rota2", url: "Prod/{id}",
                defaults: new { controller="Produto",action="Detalhe"});

            routes.MapRoute(name: "Rota3", url: "Prod/{id}/{categoria}",
                defaults: new { controller = "Produto", action = "Detalhe2" });


            routes.MapRoute(
                name: "Rota4",
                url: "{controller}/{action}/{id}/{cor}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}