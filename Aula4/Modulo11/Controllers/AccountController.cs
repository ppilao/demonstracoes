﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Modulo11.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string usuario, string senha)
        {
            Session["usuario"] = usuario;

            var u = (string)Session["usuario"];



            if (Membership.ValidateUser(usuario,senha))
                return RedirectToAction("Index", "Home");
            
            ViewBag.message = "usuário ou senha inválido";
            return View();
        }

    }
}
