﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;

namespace Aula4.Modulo9.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        [OutputCache(Duration=0)]
        public ActionResult Index()
        {
            var nomes =(string[]) MemoryCache.Default.Get("nomes");
            if (nomes==null)
            {
                nomes=new string[] { "Paulo", "Thales", "Agnes" };
                MemoryCache.Default.Add("nomes", nomes, DateTime.Now.AddMinutes(1));
            }
            ViewBag.nomes = nomes;
            return View();
        }

        public string Mensagem()
        {
            return string.Format("Mensagem enviada via Ajax às {0}",
                DateTime.Now.ToLongTimeString());
        }
        [OutputCache(Duration=0)]
        public PartialViewResult PartialViewTeste()
        {
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            System.Threading.Thread.Sleep(100);
            ViewBag.message = MostrarHorario();
            return PartialView();
        }

        private string MostrarHorario()
        {
            return  string.Format("Mensagem enviada via Ajax às {0}",
                DateTime.Now.ToLongTimeString());
        }
        [OutputCache(Duration=60)]
        public ActionResult TesteCache(int id)
        {
            System.Threading.Thread.Sleep(10000);
            ViewBag.message = MostrarHorario()+"===>>> "+id;
            return View();
        }
    }
}
