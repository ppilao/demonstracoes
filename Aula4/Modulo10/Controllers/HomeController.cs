﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Modulo10.Controllers
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public IEnumerable<Usuario> Usuarios()
        {
            yield return new Usuario { Id = 1, Nome = "Paulo", Email = "paulo@hotmail.com" };
            yield return new Usuario { Id = 2, Nome = "Thales", Email = "thales@hotmail.com" };
            yield return new Usuario { Id = 3, Nome = "Agnes", Email = "agnes@hotmail.com" };
        }
    }

    public class HomeController : Controller
    {
        public string IncluirUsuario(Usuario usuario)
        {
            return "ok";
        }
        public ActionResult Index()
        {
            return View();
        }

        public string BuscarUsuario(int id)
        {
            return new Usuario().Usuarios().FirstOrDefault(x => x.Id == id).Nome;
        }
        public JsonResult Grid()
        {
            var usuarios = new Usuario().Usuarios();
            return Json(JsonConvert.SerializeObject(usuarios), 
                JsonRequestBehavior.AllowGet);
        }

    }
}
